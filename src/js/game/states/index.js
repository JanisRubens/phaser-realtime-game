export {default as Boot} from './boot';
export {default as Load} from './load';
export {default as Menu} from './menu';
export {default as Play} from './play';
export {default as Lose} from './lose';
export {default as Win}  from './win';