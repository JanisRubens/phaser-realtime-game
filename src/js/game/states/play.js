import * as utils       from '../utils';
import Player           from '../player';

export default class Play extends Phaser.State {

    create() {
        /* Move this to boot state */
        // this.game.stage.disableVisibilityChange = true;
        // this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        // this.game.scale.setShowAll();
        // window.addEventListener('resize', () => {
        //     this.game.scale.refresh();
        // });
        // this.game.scale.refresh();

        /* Move this to boot state */

        let startX = Math.round(Math.random()*(this.game.canvas.width / 2));
		let startY = Math.round(Math.random()*(this.game.canvas.height / 2));
        console.log(startX, startY)

        this.localPlayer = new Player({
            game: this.game,
            x: startX,
            y: startY,
            details: {id: this.game.socket.id}
        })
        this.localPlayer.create();

        this.remotePlayers = [];

        this.startSocketEvents();
    }

    update() {
        this.localPlayer.update(true);
        var i;
        for (i = 0; i < this.remotePlayers.length; i++) {
            this.remotePlayers[i].update(false);
        };
    }

    startSocketEvents() {
        this.game.socket.emit('CONNECTING_TO_GAME_SERVER');
        this.game.socket.on('SUCCESSFUL_CONNECTION', this.onConnection.bind(this) );
        this.game.socket.on('DISCONNECT', this.onDisconnect.bind(this) );
        this.game.socket.on('NEW_PLAYER_JOINED', this.onNewPlayerJoin.bind(this) );
        this.game.socket.on('PLAYER_UPDATE', this.onPlayerUPdate.bind(this) );
    }

    onConnection(){
        console.log("connected to server ");

        this.game.socket.emit('NEW_PLAYER', {x: this.localPlayer.getX(), y: this.localPlayer.getY()})
    }
    onDisconnect(data){
        var removePlayer = utils.findByID(data.id, this.remotePlayers);
            removePlayer.destroySprite();
        console.log(removePlayer);
        if (!removePlayer) return;

        this.remotePlayers.splice(this.remotePlayers.indexOf(removePlayer), 1);
    }
    onNewPlayerJoin(data){
        console.log("New player connected: "+data.id);

        // Initialise the new player
        var newPlayer = new Player({
            game: this.game,
            x: data.x,
            y: data.y,
            details: {id: data.id}
        });
        // Add new player to the remote players array
        this.remotePlayers.push(newPlayer);
        console.log(newPlayer)
        var i;
        for (i = 0; i < this.remotePlayers.length; i++) {
            this.remotePlayers[i].create();
        };
    }
    onPlayerUPdate(data){
        var updatePlayer = utils.findByID(data.id, this.remotePlayers);
        updatePlayer.setX(data.x);
        updatePlayer.setY(data.y);
    }

}