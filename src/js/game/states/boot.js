export default class Boot extends Phaser.State {

    preload() {
        this.game.stage.backgroundColor = '#000';
                    //  This sets a limit on the up-scale
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.setShowAll();
        window.addEventListener('resize', () => {
            this.scale.refresh();
        });
        this.scale.refresh();

        this.stage.disableVisibilityChange = true;
        // this.load.image('loaderBg', 'img/loader-bg.png');
        // this.load.image('loaderBar', 'img/loader-bar.png');
    }

    create() {

        this.game.physics.startSystem(Phaser.Physics.ARCADE);

        // this.game.socket.emit("GAME_BOOT_START", {gameID: "randomid"});
        // this.game.socket.on(`GAME_${'randomid'}_DETAILS`, ( (data) => {
        //     console.log(data)
        //     this.game.gameID = data.id;
        //     this.game.players = data.players;
        //     this.state.start('Load');
        // }))
        this.state.start('Load');
    }

}