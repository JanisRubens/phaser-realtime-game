//import dependencies
//Phaser gets importend from public/vendor

//might call it game.js
import SocketService from '../modules/SocketService';

//import game state files
import * as states from './states';


const GAME = new Phaser.Game(640, 480, Phaser.AUTO, "gameDiv");

console.log(SocketService.socket)

GAME.socket = SocketService.socket;

Object.keys(states).forEach(state => GAME.state.add(state, states[state]));

GAME.state.start('Boot');