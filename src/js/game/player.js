export default class Player extends Phaser.Sprite {

    constructor({ game, x, y, details }) {
        super(game, x, y);
        //Closure vars
        var x = x;
        var y = y;
        var details = details;
        this.game = game;
        this.anchor.setTo(0.5);
        this.scale.setTo(0.8);

        this.getX = function() {
            return x;
        }

        this.getY = function() {
            return y;
        }

        this.setX  = function(newX) {
            x = newX;
        }

        this.setY = function(newY) {
            y = newY;
        }

        this.getDetails = function() {
            return details;
        }

    }

    create() {
        this.sprite = this.game.add.graphics(0, 0);

        // graphics.lineStyle(2, 0xffd900, 1);

        this.sprite.beginFill(0xFF0000, 1);
        this.sprite.drawCircle(this.getX(), this.getY(), 25);
    }

    update( isThisLocal ) {
        this.sprite.x = this.getX();
        this.sprite.y = this.getY();
        if (isThisLocal) {
            this.game.socket.emit('CLIENT_UPDATE', {
                id: this.getDetails().id,
                x: this.getX(),
                y: this.getY()
            })
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT))
            {
                this.setX( this.getX() - 4 );
            }
            else if (this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT))
            {
                this.setX( this.getX() + 4 );
            }

            if (this.game.input.keyboard.isDown(Phaser.Keyboard.UP))
            {
                this.setY( this.getY() - 4 );
            }
            else if (this.game.input.keyboard.isDown(Phaser.Keyboard.DOWN))
            {
                this.setY( this.getY() + 4 );
            }
        }
    }

    destroySprite() {
        this.sprite.destroy();
    }


}