var Player = require('../../helpers/Player');
var PlayerService = require('../../helpers/PlayerService');

var SocketService = function( io ) {
    io.on('connection', initOnConnectionEvents )
}

function initOnConnectionEvents( client ) {
    console.log("New player has connected: "+client.id);

	client.on('CONNECTING_TO_GAME_SERVER', ( () => {
		client.emit('SUCCESSFUL_CONNECTION');
	}))

	// Listen for client disconnected
	client.on("disconnect", onClientDisconnect);

	// Listen for new player message
	client.on("NEW_PLAYER", onNewPlayer);

	// Listen for move player message
	client.on("CLIENT_UPDATE", onPlayerUpdate);
}

function onClientDisconnect() {
	console.log("Player has disconnected: "+this.id);

	var removePlayer = PlayerService.findPlayerByID(this.id);

	// Player not found
	if (!removePlayer) {
		console.log("Player not found on disconnect: "+this.id);
		return;
	};

	// Remove player from players array
	PlayerService.players.splice(PlayerService.players.indexOf(removePlayer), 1);

	// Broadcast removed player to connected socket clients
	this.broadcast.emit("DISCONNECT", {id: this.id});
}

function onNewPlayer( data ) {
	console.log(data.x, data.y)
	// Create a new player
	var newPlayer = new Player({
		startX: data.x,
		startY: data.y, 
		details: { id: this.id }
	});
	console.log( "this.id", this.id, newPlayer.getX(), newPlayer.getY())

	// Broadcast new player to connected socket clients
	this.broadcast.emit("NEW_PLAYER_JOINED", {id: newPlayer.getDetails().id, x: newPlayer.getX(), y: newPlayer.getY()});

	// Send existing players to the new player
	var i, existingPlayer;
	for (i = 0; i < PlayerService.players.length; i++) {
	 	existingPlayer = PlayerService.players[i];
	 	this.emit("NEW_PLAYER_JOINED", {id: existingPlayer.getDetails().id, x: existingPlayer.getX(), y: existingPlayer.getY()});
	 };
		
	// Add new player to the player array;
	PlayerService.addPlayer( newPlayer );

}

function onPlayerUpdate(data) {
		// Find player in array
	var movePlayer = PlayerService.findPlayerByID(this.id);

	// Player not found
	if (!movePlayer) {
		console.log("Player not found on update: "+this.id);
		return;
	};

	// Update player position
	movePlayer.setX(data.x);
	movePlayer.setY(data.y);

	// Broadcast updated position to connected socket clients
	this.broadcast.emit("PLAYER_UPDATE", {id: movePlayer.getDetails().id, x: movePlayer.getX(), y: movePlayer.getY()});
}

module.exports = SocketService;