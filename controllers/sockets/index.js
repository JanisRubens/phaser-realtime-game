var socketService = require( './socketService')

var socketHandler = function( io ) {
    
    // Configure Socket.IO
		// Only use WebSockets
		//io.set("transports", ["websocket"]);

		// Restrict log output
		// /io.set("log level", 2);

    socketService( io );
}

module.exports = socketHandler