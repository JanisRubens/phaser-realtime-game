var express         = require('express');
var expressLayouts  = require('express-ejs-layouts');
var bodyParser      = require('body-parser');
var app             = express();
var port            = process.env.PORT || 8080;
var path            = require('path');
var routeHandler    = require( './controllers');
var socketHandler   = require( './controllers/sockets')
var http            = require('http').Server(app);
var io              = require("socket.io")(http);

// use ejs and express layouts
app.set('view engine', 'ejs');
app.use(expressLayouts);
app.set('views', path.join(__dirname, '/views'));

// use body parser
app.use(bodyParser.urlencoded({ extended: true }));

// set static files (css and images, etc) location
app.use(express.static(path.join(__dirname, 'public')));

//set up socket connection
socketHandler( io );

//set up site routes
routeHandler( app );

// start the server
http.listen(port, () => {console.log("listening on port: " + port)});

/*
#ADD THIS TO README

1. User connects:
    A) We add user to player Object in PlayerService singletone;
    B) We respond with a lobby view;
2. Lobby view contains: Available room list to join or option to create your own room;
3. Room contains, id, name, players. Room view. Should have an option to Start the game;


Sockets events required:

on.connection
on.createRoom
on.newRoom
on.joinRoom
on.hasJoinedTheRoom
on.disconnect
on.

*/