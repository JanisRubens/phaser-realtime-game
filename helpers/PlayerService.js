var PlayerService = {
    players: [],
    addPlayer: ( ( player ) => {
        PlayerService.players.push( player );
    }),
    removePlayer: ( () => {

    }),
    findPlayerByID( id ) {
        var i;
        for (i = 0; i < PlayerService.players.length; i++) {
            if (PlayerService.players[i].getDetails().id == id)
                return PlayerService.players[i];
        };
	
	    return false;
    }

}

module.exports = PlayerService;