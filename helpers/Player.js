var Player = function({startX, startY, details}) {
    var x = startX,
        y = startY,
        details = details
    
    var getX = function() {
        return x;
    };

    var getY = function() {
        return y;
    };

    var setX = function(newX) {
        x = newX;
    };

    var setY = function(newY) {
        y = newY;
    };

    var getDetails = function() {
        return details;
    };

    return {
        getX: getX,
        getY: getY,
        setX: setX,
        setY: setY,
        getDetails: getDetails
    }
};

module.exports = Player;